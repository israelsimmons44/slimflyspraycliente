<?php
require 'vendor/autoload.php';
require 'models/HeaderTicket.php';
require 'models/Bitacora.php';

use Illuminate\Database\Capsule\Manager as DB;

$resolver = new Illuminate\Database\ConnectionResolver;

$resolver->setDefaultConnection('default');
$algo = new Illuminate\Container\Container();
$factory = new Illuminate\Database\Connectors\ConnectionFactory($algo);

$connection = $factory->make(include('config.php'));
$appConfig = include('appConfig.php');
 
$resolver->addConnection('default', $connection);
 
Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);

$app = new \Slim\Slim();
$app->debug = true;
$app->config([
	'templates.path'=>'views',
]);

$DB = new DB();

$DB->addConnection(include('config.php'));
$DB->setAsGlobal();
$conn = $DB->connection();

$app->get('/test', function() use($app, $appConfig){
	$curl = new \Curl\Curl();
	$curl->get($appConfig['URL_API'].'/test');
	$data = json_decode($curl->response);	
	if($data->test->result === true){
		$mensaje = "El servidor ".$appConfig['URL_API']." está accesible";
	}else{
		$mensaje = "El servidor está mal configurado";
	}

	$app->render('prueba.php', compact('mensaje'));
	
});

$app->get('/sync/header', function() use ($app, $appConfig){
	
	try{

		$curl = new \Curl\Curl();
		$curl->get($appConfig['URL_API'].'/getData');
		$data = json_decode($curl->response);
		$bitacora = array();
		if ($data){
			foreach ($data->tasks as $key => $value) {
				$task = false;
				$upd = false;
				$data->tasks[$key]->procStatus = 'Sin Cambios';
				//print_r($value->task_id);
				$obj = HeaderTicket::select('*')->where('task_id', '=', $value->task_id)->first();
				
				if (isset($obj->id))
				$task = HeaderTicket::find($obj->id);
				//print_r($task);die();
				if ($task){
					if ($task->exists()){
						foreach ((Array)$value as $i => $v) {
							if ($i!='procStatus')
							if ($task->{$i} != $v){
								$bitacora['modelo'] = 'ticketWeb_hdr';
								$bitacora['modelo_id'] = $task->task_id;
								$bitacora['campo'] = $i;
								$bitacora['valor'] = $task->{$i};
								$task->{$i} = $v;	
								
								$bitacora['valor_nuevo'] = $task->{$i};
								Bitacora::create($bitacora);
								$upd = true;
							}
						}
						$task->save();
						if ($upd === true){
							$data->tasks[$key]->procStatus = 'Actualizado';
							$upd = false;	
						}
						
					}else{
						$var = HeaderTicket::create((Array)$value);	
						$data->tasks[$key]->procStatus = 'Creado';
						$bitacora['modelo'] = 'ticketWeb_hdr';
						$bitacora['modelo_id'] = $var->task_id;
						$bitacora['campo'] = 'Registro Nuevo';
						$bitacora['valor_nuevo'] = $var->toJson();
						Bitacora::create($bitacora);

					}	
				}else{
					$var = HeaderTicket::create((Array)$value);	
					$data->tasks[$key]->procStatus = 'Creado';					
					$bitacora['modelo'] = 'ticketWeb_hdr';
					$bitacora['modelo_id'] = $var->task_id;
					$bitacora['campo'] = 'Registro Nuevo';
					$bitacora['valor_nuevo'] = $var->toJson();
					Bitacora::create($bitacora);
				}

			}
			$app->render('resultHeader.php', compact('data'));	
		}else{
			throw new Exception("Hubo un error recuperando los datos del WebService", 666);
			
		}
		
	}catch(Exception $e){
		//echo "ocurrio un error";
		echo "<pre>";
		print_r($e);
		echo "</pre>";
	}
	
});

$app->get('/sync/header/data', function() use ($app, $appConfig){
	
	try{

		$curl = new \Curl\Curl();
		$curl->get($appConfig['URL_API'].'/getData');
		$data = json_decode($curl->response);

		echo "<pre>";
		print_r($data);
		echo "</pre>";
		
	}catch(Exception $e){
		//echo "ocurrio un error";
		echo "<pre>";
		print_r($e);
		echo "</pre>";
	}
	
});

$app->run();

