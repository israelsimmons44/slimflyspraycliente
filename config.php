<?php
return array(
	'host' => 'localhost',
	'database' => 'flyspray_sync',
	'username' => 'root',
	'password' => 'root',
	'collation' => 'utf8_general_ci',
	'driver' => 'mysql',
	'charset' => 'utf8',
	'prefix' => '',
);