CREATE TABLE IF NOT EXISTS `bitacora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(20) NOT NULL,
  `modelo_id` int(11) DEFAULT NULL,
  `campo` varchar(20) DEFAULT NULL,
  `valor` text,
  `valor_nuevo` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `ticketWeb_hdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(10) unsigned NOT NULL,
  `project_id` int(3) NOT NULL,
  `project_title` varchar(100) NOT NULL,
  `opened_by` int(3) NOT NULL,
  `opened_by_name` varchar(32) NOT NULL,
  `task_type` int(3) NOT NULL,
  `tasktype_name` varchar(40) NOT NULL,
  `item_status` int(3) NOT NULL,
  `status_name` varchar(40) NOT NULL,
  `date_opened` int(11) NOT NULL,
  `date_closed` int(11) NOT NULL,
  `task_severity` int(3) NOT NULL,
  `task_severity_desc` varchar(40) NOT NULL,
  `is_closed` tinyint(1) NOT NULL,
  `closed_by` varchar(32) DEFAULT NULL,
  `closure_comment` text NOT NULL,
  `resolution_name` varchar(40) DEFAULT NULL,
  `percent_complete` int(3) NOT NULL,
  `priority` varchar(30) NOT NULL,
  `item_summary` varchar(100) NOT NULL,
  `detailed_desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE `flyspray_notifications` CHANGE `notify_id` `notify_id` INT(10) NOT NULL; 
ALTER TABLE `flyspray_notifications` DROP PRIMARY KEY; 
ALTER TABLE `flyspray_notifications` ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST; 

ALTER TABLE `flyspray_assigned` CHANGE `assigned_id` `assigned_id` INT(10) NOT NULL; 
ALTER TABLE `flyspray_assigned` DROP PRIMARY KEY; 
ALTER TABLE `flyspray_assigned` ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST; 

ALTER TABLE `flyspray_admin_requests` CHANGE `request_id` `request_id` INT(5) NOT NULL; 
ALTER TABLE `flyspray_admin_requests` DROP PRIMARY KEY; 
ALTER TABLE `flyspray_admin_requests` ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST; 

