<?php
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Curl\Curl;


require __DIR__.'/vendor/autoload.php';
require 'models/HeaderTicket.php';
require 'models/Comments.php';
require 'models/Notifications.php';
require 'models/Assigned.php';
require 'models/AdminRequests.php';
require 'models/Bitacora.php';

use Illuminate\Database\Capsule\Manager as DB;

$resolver = new Illuminate\Database\ConnectionResolver;

$resolver->setDefaultConnection('default');
$algo = new Illuminate\Container\Container();
$factory = new Illuminate\Database\Connectors\ConnectionFactory($algo);

$connection = $factory->make(include('config.php'));
$appConfig = include('appConfig.php');
 
$resolver->addConnection('default', $connection);
 
Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);
$application = new Application();

$application
	->register('getHeader')
	->setDescription('Comando para actualización del header')
	->setCode(function(InputInterface $input, OutputInterface $output) use($application, $appConfig){
		$output->writeln('Inicio del proceso de actualización de Header: '.date('d-m-Y H:i:s'));

		try{

		$curl = new \Curl\Curl();
		$curl->get($appConfig['URL_API'].'/getData');
		$output->writeln('Datos recibidos: '.date('d-m-Y H:i:s'));
		$data = json_decode($curl->response);
		$bitacora = array();
		if ($data){
			foreach ($data->tasks as $key => $value) {
				$task = false;
				$upd = false;
				$data->tasks[$key]->procStatus = 'Sin Cambios';
				//print_r($value->task_id);
				$obj = HeaderTicket::select('*')->where('task_id', '=', $value->task_id)->first();
				
				if (isset($obj->id))
				$task = HeaderTicket::find($obj->id);
				//print_r($task);die();
				if ($task){
					if ($task->exists()){
						foreach ((Array)$value as $i => $v) {
							if ($i!='procStatus')
							if ($task->{$i} != $v){
								$bitacora['modelo'] = 'ticketWeb_hdr';
								$bitacora['modelo_id'] = $task->task_id;
								$bitacora['campo'] = $i;
								$bitacora['valor'] = $task->{$i};
								$task->{$i} = $v;	
								
								$bitacora['valor_nuevo'] = $task->{$i};
								Bitacora::create($bitacora);
								$upd = true;
							}
						}
						$task->save();
						if ($upd === true){
							$data->tasks[$key]->procStatus = 'Actualizado';
							$output->writeln('Task #'.$data->tasks[$key]->task_id. ' Actualizado' );
							$upd = false;	
						}
						
					}else{
						$var = HeaderTicket::create((Array)$value);	
						$data->tasks[$key]->procStatus = 'Creado';
						$bitacora['modelo'] = 'ticketWeb_hdr';
						$bitacora['modelo_id'] = $var->task_id;
						$bitacora['campo'] = 'Registro Nuevo';
						$bitacora['valor_nuevo'] = $var->toJson();
						Bitacora::create($bitacora);
						$output->writeln('Task #'.$data->tasks[$key]->task_id. ' Creado' );

					}	
				}else{
					$var = HeaderTicket::create((Array)$value);	
					$data->tasks[$key]->procStatus = 'Creado';					
					$bitacora['modelo'] = 'ticketWeb_hdr';
					$bitacora['modelo_id'] = $var->task_id;
					$bitacora['campo'] = 'Registro Nuevo';
					$bitacora['valor_nuevo'] = $var->toJson();
					Bitacora::create($bitacora);
					$output->writeln('Task #'.$data->tasks[$key]->task_id. ' Creado' );
				}

			}
			//$app->render('resultHeader.php', compact('data'));	
		}else{
			throw new Exception("Hubo un error recuperando los datos del WebService", 666);
			
		}
		
	}catch(Exception $e){
		//echo "ocurrio un error";
		echo "<pre>";
		print_r($e);
		echo "</pre>";
	}
	$output->writeln('Fin del proceso de actualización de Header: '.date('d-m-Y H:i:s'));
	});


$application
	->register('getComments')
	->setDescription('Comando para actualización de la tabla comments')
	->setCode(function(InputInterface $input, OutputInterface $output) use($application, $appConfig){
		$output->writeln('Inicio del proceso de actualización de comments: '.date('d-m-Y H:i:s'));

		$curl = new \Curl\Curl();
		$curl->get($appConfig['URL_API'].'/sync/comments');
		$output->writeln('Datos recibidos: '.date('d-m-Y H:i:s'));
		$data = json_decode($curl->response);

		$output->writeln('Cantidad de comentarios Obtenidos: '.count($data->sync->comments));

		foreach ($data->sync->comments as $key => $value) {
			$upd = false;
			$obj = Comments::select('*')->where('comment_id', '=', $value->comment_id)->first();

			if ($obj){
				foreach ((Array)$value as $i => $v) {
					if ($obj->{$i} != $v){
						$bitacora['modelo'] = 'comments';
						$bitacora['modelo_id'] = $obj->comment_id;
						$bitacora['campo'] = $i;
						$bitacora['valor'] = $obj->{$i};
						$obj->{$i} = $v;
						$bitacora['valor_nuevo'] = $obj->{$i};
						Bitacora::create($bitacora);
						$upd = true;
					}
				}
				if ($upd){
					$obj->save();	
					$output->writeln('Comment #'.$data->sync->comments[$key]->comment_id. ' Actualizado' );
				}
				
			}else{
				$var = Comments::create((Array)$value );
				$bitacora['modelo'] = 'comments';
				$bitacora['modelo_id'] = $var->comment_id;
				$bitacora['campo'] = 'Registro Nuevo';
				$bitacora['valor_nuevo'] = $var->toJson();
				Bitacora::create($bitacora);
				$output->writeln('Comment #'.$data->sync->comments[$key]->comment_id. ' Creado' );
			}
			

		}

		$output->writeln('Fin del proceso de actualización de la tabla comments: '.date('d-m-Y H:i:s'));
	});


$application
	->register('getNotifications')
	->setDescription('Comando para actualización de la tabla notifications')
	->setCode(function(InputInterface $input, OutputInterface $output) use($application, $appConfig){
		$output->writeln('Inicio del proceso de actualización de notifications: '.date('d-m-Y H:i:s'));

		$curl = new \Curl\Curl();
		$curl->get($appConfig['URL_API'].'/sync/notifications');
		$output->writeln('Datos recibidos: '.date('d-m-Y H:i:s'));
		$data = json_decode($curl->response);

		$output->writeln('Cantidad de registros Obtenidos: '.count($data->sync->notifications));

		foreach ($data->sync->notifications as $key => $value) {
			$upd = false;
			$obj = Notifications::select('*')->where('notify_id', '=', $value->notify_id)->first();

			if ($obj){
				foreach ((Array)$value as $i => $v) {
					if ($obj->{$i} != $v){
						$bitacora['modelo'] = 'notifications';
						$bitacora['modelo_id'] = $obj->notify_id;
						$bitacora['campo'] = $i;
						$bitacora['valor'] = $obj->{$i};
						$obj->{$i} = $v;
						$bitacora['valor_nuevo'] = $obj->{$i};
						Bitacora::create($bitacora);
						$upd = true;
					}
				}
				if ($upd){
					$obj->save();
					$output->writeln('Notification #'.$data->sync->notifications[$key]->notify_id. ' Actualizado' );	
				}
				
			}else{
				$var = Notifications::create((Array)$value );
				$bitacora['modelo'] = 'notifications';
				$bitacora['modelo_id'] = $var->notify_id;
				$bitacora['campo'] = 'Registro Nuevo';
				$bitacora['valor_nuevo'] = $var->toJson();
				Bitacora::create($bitacora);
				$output->writeln('Notification #'.$data->sync->notifications[$key]->notify_id. ' Creado' );
			}
			

		}

		$output->writeln('Fin del proceso de actualización de la tabla notifications: '.date('d-m-Y H:i:s'));
	});

$application
	->register('getAssigned')
	->setDescription('Comando para actualización de la tabla assigned')
	->setCode(function(InputInterface $input, OutputInterface $output) use($application, $appConfig){
		$output->writeln('Inicio del proceso de actualización de assigned: '.date('d-m-Y H:i:s'));

		$curl = new \Curl\Curl();
		$curl->get($appConfig['URL_API'].'/sync/assigned');
		$output->writeln('Datos recibidos: '.date('d-m-Y H:i:s'));
		$data = json_decode($curl->response);

		$output->writeln('Cantidad de registros Obtenidos: '.count($data->sync->assigned));

		foreach ($data->sync->assigned as $key => $value) {
			$upd = false;
			$obj = Assigned::select('*')->where('assigned_id', '=', $value->assigned_id)->first();

			if ($obj){
				foreach ((Array)$value as $i => $v) {
					if ($obj->{$i} != $v){
						$bitacora['modelo'] = 'assigned';
						$bitacora['modelo_id'] = $obj->assigned_id;
						$bitacora['campo'] = $i;
						$bitacora['valor'] = $obj->{$i};
						$obj->{$i} = $v;
						$bitacora['valor_nuevo'] = $obj->{$i};
						Bitacora::create($bitacora);
						$upd = true;
					}
				}
				if ($upd){
					$obj->save();
					$output->writeln('Assigned #'.$data->sync->assigned[$key]->assigned_id. ' Actualizado' );	
				}
				
			}else{
				$var = Assigned::create((Array)$value );
				$bitacora['modelo'] = 'flyspray_assigned';
				$bitacora['modelo_id'] = $var->assigned_id;
				$bitacora['campo'] = 'Registro Nuevo';
				$bitacora['valor_nuevo'] = $var->toJson();
				Bitacora::create($bitacora);
				$output->writeln('Assigned #'.$data->sync->assigned[$key]->assigned_id. ' Creado' );
			}
			

		}

		$output->writeln('Fin del proceso de actualización de la tabla notifications: '.date('d-m-Y H:i:s'));
	});

$application
	->register('getAdminRequest')
	->setDescription('Comando para actualización de la tabla admin_requests')
	->setCode(function(InputInterface $input, OutputInterface $output) use($application, $appConfig){
		$output->writeln('Inicio del proceso de actualización de assigned: '.date('d-m-Y H:i:s'));

		$curl = new \Curl\Curl();
		$curl->get($appConfig['URL_API'].'/sync/admin_requests');
		$output->writeln('Datos recibidos: '.date('d-m-Y H:i:s'));
		$data = json_decode($curl->response);

		$output->writeln('Cantidad de registros Obtenidos: '.count($data->sync->admin_requests));

		foreach ($data->sync->admin_requests as $key => $value) {
			$upd = false;
			$obj = AdminRequests::select('*')->where('request_id', '=', $value->request_id)->first();

			if ($obj){
				foreach ((Array)$value as $i => $v) {
					if ($obj->{$i} != $v){
						$bitacora['modelo'] = 'admin_requests';
						$bitacora['modelo_id'] = $obj->request_id;
						$bitacora['campo'] = $i;
						$bitacora['valor'] = $obj->{$i};
						$obj->{$i} = $v;
						$bitacora['valor_nuevo'] = $obj->{$i};
						Bitacora::create($bitacora);
						$upd = true;
					}
				}
				if ($upd){
					$obj->save();
					$output->writeln('Admin Request #'.$data->sync->admin_requests[$key]->request_id. ' Actualizado' );	
				}
				
			}else{
				$var = AdminRequests::create((Array)$value );
				$bitacora['modelo'] = 'admin_requests';
				$bitacora['modelo_id'] = $var->request_id;
				$bitacora['campo'] = 'Registro Nuevo';
				$bitacora['valor_nuevo'] = $var->toJson();
				Bitacora::create($bitacora);
				$output->writeln('Admin Request #'.$data->sync->admin_requests[$key]->request_id. ' Creado' );
			}
			

		}

		$output->writeln('Fin del proceso de actualización de la tabla notifications: '.date('d-m-Y H:i:s'));
	});

$application->run();