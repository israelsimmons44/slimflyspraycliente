<?php 

use Illuminate\Database\Eloquent\Model as Model;

class Comments extends Model {
	public $timestamps = false;
	protected $table = 'flyspray_comments';
	protected $fillable = [
		'comment_id',
		'task_id',
		'date_added',
		'user_id',
		'comment_text',
		'last_edited_time',

	];
}