<?php 

use Illuminate\Database\Eloquent\Model as Model;

class AdminRequests extends Model {
	public $timestamps = false;
	protected $table = 'flyspray_admin_requests';
	protected $fillable = [
		'request_id',
		'project_id',
		'task_id',
		'submitted_by',
		'request_type',
		'reason_given',
		'time_submitted',
		'resolved_by',
		'time_resolved',
		'deny_reason',
	];
}

