<?php 

use Illuminate\Database\Eloquent\Model as Model;

class Assigned extends Model {
	public $timestamps = false;
	protected $table = 'flyspray_assigned';
	protected $fillable = [
		'assigned_id',
		'task_id',
		'user_id'
	];
}

