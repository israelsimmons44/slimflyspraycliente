<?php 

use Illuminate\Database\Eloquent\Model as Model;

class HeaderTicket extends Model {
	public $timestamps = false;
	protected $primaryKey = 'id';
	protected $table = 'ticketWeb_hdr';
	protected $fillable = [
		'task_id',
		'project_id',
		'project_title',
		'opened_by',
		'opened_by_name',
		'task_type',
		'tasktype_name',
		'item_status',
		'status_name',
		'date_opened',
		'date_closed',
		'task_severity',
		'task_severity_desc',
		'is_closed',
		'closed_by',
		'closure_comment',
		'resolution_name',
		'percent_complete',
		'priority',
		'item_summary',
		'detailed_desc',
		'due_date'
	];
}