<?php 

use Illuminate\Database\Eloquent\Model as Model;

class Bitacora extends Model {
	//public $timestamps = false;
	protected $primaryKey = 'id';
	protected $table = 'bitacora';
	protected $fillable = [
		'modelo',
		'modelo_id',
		'campo',
		'valor',
		'valor_nuevo',

	];
}