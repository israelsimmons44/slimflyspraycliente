<?php 

use Illuminate\Database\Eloquent\Model as Model;

class Notifications extends Model {
	public $timestamps = false;
	protected $table = 'flyspray_notifications';
	protected $fillable = [
		'notify_id',
		'task_id',
		'user_id'
	];
}

